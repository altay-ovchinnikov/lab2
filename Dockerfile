FROM alpine:3.19

WORKDIR /app


COPY requirements.txt .

RUN apk add py-pip
RUN pip install -r requirements.txt --break-system-packages --no-cache-dir

RUN addgroup -g 2000 user && adduser -u 2000 -G user -s /bin/sh -D user

USER user

COPY --chown=user:user . .

RUN python3 manage.py migrate blog && \
    chmod 700 init.sh

ENTRYPOINT ./init.sh
